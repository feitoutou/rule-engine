package ezpay.model;

import java.math.BigDecimal;

public class Amount {
	public BigDecimal value;
	public String currency;
}
