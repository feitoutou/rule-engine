package ezpay.model;

import java.util.List;

public class Subscription {

	public String id;
	public Amount amount;
	public String subscriptionType;
	
	public Integer payDay;
	
	public String endDate;
	public List<String> invoiceDates;
}
