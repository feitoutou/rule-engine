package ezpay.controller;

import static spark.Spark.before;
import static spark.Spark.post;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import ezpay.model.Subscription;
import ezpay.utils.JsonTransformer;
import spark.Spark;

public class SubscriptionController {
	public static void main( String[] args )
    {
    	Spark.staticFileLocation("/public");
    	before((request, response) -> response.type("application/json"));

        post("/subscription/new", (request, response) -> {
        	ObjectMapper mapper = new ObjectMapper();
        	mapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
        	
        	Subscription subscription = null;
        	try {
        		subscription = mapper.readValue(request.body(), Subscription.class);
        		if (subscription.amount.value == null || subscription.amount.value.doubleValue() <= 0) {
        			response.status(400);
        			return "Amount is not valid.";
        		}
        		if (subscription.payDay <= 0 
        				|| (subscription.payDay > 7 && "WEEKLY".equals(subscription.subscriptionType)) 
        				|| (subscription.payDay > 31 && "MONTHLY".equals(subscription.subscriptionType))) {
        			response.status(400);
        			return "Pay day is not valid.";
        		}
        	} catch (Exception e) {
        		response.status(400);
        		return "Invalid data";
        	}
        	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        	Calendar endDate = Calendar.getInstance();
        	try {
        		Date parsedDate = sdf.parse(subscription.endDate);
        		endDate.setTime(parsedDate);
        	} catch (Exception e) {
        		response.status(400);
        		return "Invalid end date";
        	}
        	
        	Calendar threeMonthLater = Calendar.getInstance();
        	threeMonthLater.add(Calendar.MONTH, 3);
        	
        	if (endDate.after(threeMonthLater)) {
        		response.status(400);
        		return "End date should be within 3 months";
        	}
        	
        	subscription.invoiceDates = generateInvoiceDates(subscription.subscriptionType, subscription.payDay, endDate);
        	subscription.id = UUID.randomUUID().toString();
        	subscription.amount.currency = "AUD";
        	subscription.payDay = null;
        	subscription.endDate = null;
        	return subscription;
        }, new JsonTransformer());
    }

	private static List<String> generateInvoiceDates(String subscriptionType, Integer payDay, Calendar endDate) {
		List<String> result = new ArrayList<>();
		Calendar now = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if ("DAILY".equals(subscriptionType)) {
			while (now.before(endDate)) {
				result.add(sdf.format(now.getTime()));
				now.add(Calendar.DATE, 1);
			}
		}
		else if ("WEEKLY".equals(subscriptionType)) {
			Integer weekDayOfNow = now.get(Calendar.DAY_OF_WEEK);
			int dayDiff = 7 - Math.abs(payDay.intValue() - weekDayOfNow.intValue());
			now.add(Calendar.DATE, dayDiff);
			while (now.before(endDate)) {
				result.add(sdf.format(now.getTime()));
				now.add(Calendar.DATE, 7);
			}
		}
		else if ("MONTHLY".equals(subscriptionType)) {
			Integer monthDayOfNow = now.get(Calendar.DAY_OF_MONTH);
			if (monthDayOfNow.intValue() <= payDay.intValue()) {
				now.set(Calendar.DAY_OF_MONTH, payDay);
			} else {
				now.add(Calendar.MONTH, 1);
				now.set(Calendar.DAY_OF_MONTH, Math.min(payDay, now.getActualMaximum(Calendar.DAY_OF_MONTH)));
			}
			while (now.before(endDate)) {
				result.add(sdf.format(now.getTime()));
				now.add(Calendar.MONTH, 1);
				now.set(Calendar.DAY_OF_MONTH, Math.min(payDay, now.getActualMaximum(Calendar.DAY_OF_MONTH)));
			}
		}
		return result;
	}
}
