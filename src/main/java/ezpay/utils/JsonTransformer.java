package ezpay.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import spark.ResponseTransformer;
 
public class JsonTransformer implements ResponseTransformer {
 
    @Override
    public String render(Object model) {
    	ObjectMapper mapper = new ObjectMapper();
    	mapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
    	mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    	try {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(model);
		} catch (JsonProcessingException e) {
		}
    	return "";
    }
 
    
}