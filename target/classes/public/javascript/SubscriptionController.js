'use strict';

var app = angular.module('app', [])
.run(function ($http) {
	
});

app.controller('SubscriptionController', ['$http', function($http) {
    var self = this;
    self.subscription = {
    	"amount": {"value": 10, "currency": 'AUD'},
    	"subscription_type": "DAILY",
    	"pay_day": 1,			//which day the payment happens. Sunday-Saturday: 1-7. Month day as it is.
    	"end_date": null
    }
    
    self.result = null;
    
    self.submit = function() {
    	//browser side validation - ignored
    	$http.post("/subscription/new", self.subscription)
    	.success(function (response) {
    		self.result = response;
    		console.log(response);
    	})
    	.error(function (response) {
    		self.result = response;
    	})
    	
//    	$.ajax({
//		  type: "POST",
//		  url: "/subscription/new",
//		  data: {"test": 1, "bb": 2},
//		  success: function(r) {console.log(r)}
//		});
    }
    
}]);